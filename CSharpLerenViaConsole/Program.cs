﻿using System;

namespace CSharpLerenViaConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Crescendo World!");
            CSharpLerenViaConsole.WerkenMetGegevens.CharLerenGebruiken();
            // ShowAscii is niet static
            // eerst instantie maken
            WerkenMetGegevens werkenMetGegevens = new WerkenMetGegevens();
            werkenMetGegevens.ShowAllAsciiValues();
            Console.ReadKey();
            WerkenMetGegevens.showCultureInfo();
            WerkenMetGegevens.stringConcatenationVersuStringBuilder();
            Console.ReadKey();
        }
    }
}
