﻿using System;
using Wiskunde.Meetkunde;

namespace BeginnenMetCSharp
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine(Vormen.Opdracht(20));
            Console.WriteLine(Vormen.Opdracht(50, '*'));
            Console.WriteLine("\n\n");

            Console.WriteLine(Vormen.Opdracht(7, '-', ConsoleColor.Green));
            Console.Write(Vormen.Opdracht(1, '|'));
            Console.Write(Vormen.Opdracht(5, ' '));
            Console.WriteLine(Vormen.Opdracht(1, '|'));
            Console.Write(Vormen.Opdracht(1, '|'));
            Console.Write(Vormen.Opdracht(5, ' '));
            Console.WriteLine(Vormen.Opdracht(1, '|'));
            Console.Write(Vormen.Opdracht(1, '|'));
            Console.Write(Vormen.Opdracht(5, ' '));
            Console.WriteLine(Vormen.Opdracht(1, '|'));
            Console.Write(Vormen.Opdracht(1, '|'));
            Console.Write(Vormen.Opdracht(5, ' '));
            Console.WriteLine(Vormen.Opdracht(1, '|'));
            Console.Write(Vormen.Opdracht(1, '|'));
            Console.Write(Vormen.Opdracht(5, ' '));
            Console.WriteLine(Vormen.Opdracht(1, '|'));
            Console.WriteLine(Vormen.Opdracht(7));

            Console.WriteLine("\n");

            Console.Write(Vormen.Opdracht(0, ConsoleColor.DarkMagenta));
            Console.Write(Vormen.Opdracht(4, ' '));
            Console.WriteLine(Vormen.Opdracht(1, '-'));
            Console.Write(Vormen.Opdracht(3, ' '));
            Console.Write(Vormen.Opdracht(1, '-'));
            Console.Write(Vormen.Opdracht(1, ' '));
            Console.WriteLine(Vormen.Opdracht(1, '-'));
            Console.Write(Vormen.Opdracht(2, ' '));
            Console.Write(Vormen.Opdracht(1, '-'));
            Console.Write(Vormen.Opdracht(3, ' '));
            Console.WriteLine(Vormen.Opdracht(1, '-'));
            Console.Write(Vormen.Opdracht(1, ' '));
            Console.Write(Vormen.Opdracht(1, '-'));
            Console.Write(Vormen.Opdracht(5, ' '));
            Console.WriteLine(Vormen.Opdracht(1, '-'));
            Console.WriteLine(Vormen.Opdracht(9, '-'));

            Console.ReadKey(true);
        }
    }
}
