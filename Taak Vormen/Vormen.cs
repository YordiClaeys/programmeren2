﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    class Vormen
    {
        private ConsoleColor kleur;
        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value;
                Console.ForegroundColor = kleur;
            }
        }

        public static string Opdracht(int lengte)
        {
            return new string('-', lengte);
        }

        public static string Opdracht(int lengte, char teken)
        {
            return new string(teken, lengte);
        }

        public static string Opdracht(int lengte, ConsoleColor kleur)
        {
            Console.ForegroundColor = kleur;
            return new string('-', lengte);

        }

        public static string Opdracht(int lengte, char teken, ConsoleColor kleur)
        {
            Console.ForegroundColor = kleur;
            return new string(teken, lengte);
        }
    }
}
